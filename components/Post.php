<?php namespace Autumn\Blog\Components;

use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Response;
use Autumn\Blog\Models\Post as BlogPost;

class Post extends ComponentBase
{
    /**
     * @var Autumn\Blog\Models\Post The post model used for display.
     */
    public $post;

    /**
     * @var string Reference to the page name for linking to categories.
     */
    public $categoryPage;

    public function componentDetails()
    {
        return [
            'name' => 'Autumn.Blog post',
            'description' => 'autumn.blog::lang.settings.post_description'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title' => 'autumn.blog::lang.settings.post_slug',
                'description' => 'autumn.blog::lang.settings.post_slug_description',
                'default' => '{{ :slug }}',
                'type' => 'string'
            ],
            'categoryPage' => [
                'title' => 'autumn.blog::lang.settings.post_category',
                'description' => 'autumn.blog::lang.settings.post_category_description',
                'type' => 'dropdown',
                'default' => 'blog/category',
            ],
        ];
    }

    public function getCategoryPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function init()
    {
        $this->categoryPage = $this->page['categoryPage'] = $this->property('categoryPage');
        $this->post = $this->page['post'] = $this->loadPost();
        if (!$this->post) {
            return Response::make($this->controller->run('404'), 404);
        }
    }

    protected function loadPost()
    {
        $slug = $this->property('slug');
        $post = BlogPost::isPublished()->where('slug', $slug)->first();

        /*
         * Add a "url" helper attribute for linking to each category
         */
        if ($post && $post->categories->count()) {
            $post->categories->each(function ($category) {
                $category->setUrl($this->categoryPage, $this->controller);
            });
        }

        return $post;
    }
}
