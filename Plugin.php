<?php namespace Autumn\Blog;

use Backend;
use Controller;
use System\Classes\PluginBase;
use Autumn\Blog\Classes\TagProcessor;
use Autumn\Blog\Models\Category;
use Event;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'autumn.blog::lang.plugin.name',
            'description' => 'autumn.blog::lang.plugin.description',
            'author'      => 'Bestxp aka Ilja Muraviev, <br />original by Alexey Bobkov, Samuel Georges',
            'icon'        => 'icon-leaf',
           // 'homepage'    => 'https://github.com/autumn/blog-plugin'
        ];
    }

    public function registerComponents()
    {
        return [
            'Autumn\Blog\Components\Post'       => 'blogPost',
            'Autumn\Blog\Components\Posts'      => 'blogPosts',
            'Autumn\Blog\Components\Categories' => 'blogCategories'
        ];
    }

    public function registerPermissions()
    {
        return [
            'autumn.blog.access_posts'       => ['tab' => 'Autumn Blog', 'label' => 'autumn.blog::lang.blog.access_posts'],
            'autumn.blog.access_categories'  => ['tab' => 'Autumn Blog', 'label' => 'autumn.blog::lang.blog.access_categories'],
            'autumn.blog.access_other_posts' => ['tab' => 'Autumn Blog', 'label' => 'autumn.blog::lang.blog.access_other_posts']
        ];
    }

    public function registerNavigation()
    {
        return [
            'blog' => [
                'label'       => 'autumn.blog::lang.blog.menu_label',
                'url'         => Backend::url('autumn/blog/posts'),
                'icon'        => 'icon-leaf',
                'permissions' => ['autumn.blog.*'],
                'order'       => 400,

                'sideMenu' => [
                    'posts' => [
                        'label'       => 'autumn.blog::lang.blog.posts',
                        'icon'        => 'icon-copy',
                        'url'         => Backend::url('autumn/blog/posts'),
                        'permissions' => ['autumn.blog.access_posts']
                    ],
                    'categories' => [
                        'label'       => 'autumn.blog::lang.blog.categories',
                        'icon'        => 'icon-list-ul',
                        'url'         => Backend::url('autumn/blog/categories'),
                        'permissions' => ['autumn.blog.access_categories']
                    ],
                ]
            ]
        ];
    }

    public function registerFormWidgets()
    {
        return [
            'Autumn\Blog\FormWidgets\Preview' => [
                'label' => 'Preview',
                'code'  => 'preview'
            ]
        ];
    }

    public function boot()
    {
        /*
         * Register menu items for the autumn.Pages plugin
         */
        Event::listen('pages.menuitem.listTypes', function() {
            return [
                'blog-category' => 'Blog category',
                'all-blog-categories' => 'All blog categories'
            ];
        });

        Event::listen('pages.menuitem.getTypeInfo', function($type) {
            if ($type == 'blog-category' || $type == 'all-blog-categories')
                return Category::getMenuTypeInfo($type);
        });

        Event::listen('pages.menuitem.resolveItem', function($type, $item, $url, $theme) {
            if ($type == 'blog-category' || $type == 'all-blog-categories')
                return Category::resolveMenuItem($item, $url, $theme);
        });
    }
}
