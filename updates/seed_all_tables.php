<?php namespace Autumn\Blog\Updates;

use Autumn\Blog\Models\Category;
use October\Rain\Database\Updates\Seeder;

class SeedAllTables extends Seeder
{

    public function run()
    {
        //
        // @todo
        //
        // Add a Welcome post or something
        //

        Category::create([
            'name' => trans('autumn.blog::lang.categories.uncategorized'),
            'slug' => 'uncategorized',
        ]);
    }

}
